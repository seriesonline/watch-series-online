# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

For watching series online and movies online for free.

### How do I get set up? ###

Just visit any of the site mentioned in the list and you can easily watch or stream movies and series for free without signup.

### Contribution guidelines ###

MovieStreamingSite (https://moviestreamingsite.com)
This is my personal favorite because it does not have any ads and doesn't require you to signup before accessing the streaming services. Also, you can download the tv shows and movies to your computer if you want to watch at a later time. One con is that the developer seems pretty busy so sometimes it takes him time to upload new episodes. But one this is for sure, he must be a die hard fan of Game of Thrones because I've never seen him delay uploading any episode of the series.

MrSeriesOnline (https://MrSeriesOnline.com)
This is a rather new site. It's clean and like the above site, very user friendly and does not have any ads. I found this website when I was searching for watching series online which were taken down instantly by most of the hosts. You can watch series online and full movies online for free on this site.

FreeHDMovies (https://freehdmovies.org)
Watch free hd movies online for free on this site. This site has all the latest released movies. This site might lack in the series section but has all the movies you've ever wanted to watch online for free and that too in HD. Simple and very user friendly.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
Watch Series Online Free
https://moviestreamingsite.com